import { Directive, ElementRef, OnInit } from '@angular/core';

@Directive({
    selector: '[magicEffect]',
})
export class MagicEffectDirective implements OnInit {

    constructor(private _eRef: ElementRef) { }

    ngOnInit(): any {
        setInterval(() => {
            let a = [];
            a.push(Math.floor(Math.random() * 255));
            a.push(Math.floor(Math.random() * 255));
            a.push(Math.floor(Math.random() * 255));
            this._eRef.nativeElement.style.color = 'rgb(' + a[0] + ',' + a[1] + ',' + a[2] + ')';

        }, 100);
    }

}