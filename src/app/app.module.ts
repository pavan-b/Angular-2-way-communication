import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { LayoutComponent } from './components/layout.component';
import { InnerLayoutComponent } from './components/innerLayout.component';

import { MagicEffectDirective } from './directives/magicEffect.directive.component';

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    InnerLayoutComponent,
    MagicEffectDirective
    ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
