import { Component, Input, EventEmitter, Output } from '@angular/core';

@Component({
    selector: 'app-inner-layout',
    template: `
    <div class="component-2">
        <h2>Child-Component</h2>
        <label for="hobbies">Type here to see it in Parent </label>
        <input type="text" name='hobbies' (keyup)="hobbieEmmiter(input.value)" #input />
        <p><span *ngIf="innerText" ><b>Parent says : </b></span> {{innerText}} </p>
    </div>
    `,
    styleUrls: ['./innerLayout.component.css']
})
export class InnerLayoutComponent {
    @Input() innerText: String = '';
    @Output() onHobbiesChanged = new EventEmitter<String>();
    hobbieEmmiter(hobbies: String) {
        this.onHobbiesChanged.emit(hobbies);

    }


}