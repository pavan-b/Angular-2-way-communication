import { Component } from '@angular/core';

@Component({
    selector: 'app-layout',
    template: `
    <div class='container'>
        <div class="component-1">
        <h2>Parent-Component</h2>
        <label for="data">Type here to see it in Child </label>
        <input  name="data"  (keyUp)=0 [(ngModel)]="text"  />
        <p><span *ngIf="hobbies" ><b>Child says : </b></span> {{hobbies}} </p>
        </div>
        <br>
        <app-inner-layout [innerText]="text" (onHobbiesChanged)="hobbies = $event"></app-inner-layout>
    </div>
    `,
    styleUrls: ['./layout.component.css'],
})
export class LayoutComponent {
    text: String = '';
    hobbies: String = '';
}